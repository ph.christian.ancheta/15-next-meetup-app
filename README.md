# Meetup - React + Next JS App
A simple React App with Next JS concepts that allows users to add meetups with details to be filled up through a form. This application also uses a backend database through MongoDB Atlas with the main focus of the section targeted in exploring the Next JS.

## Setup

```
npm install
npm run start
```
